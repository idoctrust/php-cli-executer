<?php
require_once  '../vendor/autoload.php';
use iDocTrust\Exec\JsonCmdExec;

$testData = [
  "someString" => "bleh",
    "someInt" => 10,
    "someNull" => null
];

echo "\nExecuting command\n";
$returnedData = JsonCmdExec::run("grep .",  json_decode(file_get_contents('../composer.json'),true));
echo "----\n".json_encode($returnedData);
