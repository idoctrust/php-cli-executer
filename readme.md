A very simple library simplifying communicating with command line applications
expecting json objects as stdin input and pushes out results on stdout.

We assume that the application uses stderr for logging.