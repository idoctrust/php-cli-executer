<?php
namespace iDocTrust\Exec;

class JsonCmdExec {
    
    static function base64url_encode($data) { 
      return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
    } 

    private static function shell_exec_p($cmd,$inString,$cwd = null)
    {

        $description = array (  0 => array("pipe", "r"),  // stdin
            1 => array("pipe", "w"),  // stdout
            2 => array("file", "/tmp/testPipe1.log", "w"),
        );
        $pipes = array();

        $proc = proc_open ( $cmd , $description , $pipes, $cwd);
        
        if(!is_resource($proc)){
            throw new Exception("Failed opening procdess with cmd=$cmd");
        }
        
        // Unblock both streams
        stream_set_blocking ($pipes [0] , 0);
        stream_set_blocking ($pipes [1] , 0);
        
        $tries = 3;
        $outputText = "";
        while(!empty($inString)){
            //Write as much as possible
            $writtenCount = fwrite($pipes[0], $inString);
            // Now read as much as possible
            while($s=fread($pipes[1], 16384*4)){
                        $outputText.=$s;
            }
            
            // If nothing could be written, then wait a sec
            if(!$writtenCount)
            {
                sleep(1);
                $tries--;
                if(!$tries){
                    throw new Exception("Failed writing to process");
                }
            }
            // Chop off the bytes already written
            $inString = substr($inString, $writtenCount);
        }
        
        // Done writing, close input
        fclose($pipes[0]);
        
        // reading the rest of output stream
        stream_set_blocking($pipes[1], 1);
        while(!feof($pipes[1]))
        {
            $s=fread($pipes[1], 16384);
            $outputText.=$s;
        }
        fclose($pipes[1]);
        return $outputText;
    }    
    
    /**
     * @param string $command The command to be executed
     * @param array $inputData Array of data to pass to the process via stdin
     * @return The JSON decoded string coming from the process
     */
    public static function run($command,array $inputData,$cwd=null){
        $outputText = JsonCmdExec::shell_exec_p($command, JsonCmdExec::base64url_encode(json_encode($inputData)),$cwd);
        $decoded = json_decode($outputText);
        if(empty($decoded)){
            throw new \Exception("Verification failed : Output from command_line_verifier could not be parsed");
        }
        return $decoded;
    }
    
}

